import { Location } from '@angular/common';
import { FormGroup } from '@angular/forms';
import { Component } from '@angular/core';

import { DashboardService } from './../service/dashboard.service';
import { DragonModel } from './../dragoon-list/model/dragon.model';
import { DragoonCreateForm } from './dragoon-create-form/dragoon-create-form';

@Component({
  selector: 'app-dragoon-create',
  templateUrl: './dragoon-create.component.html',
  styleUrls: ['./dragoon-create.component.css']
})
export class DragoonCreateComponent {

  public formNewDragon: FormGroup;

  constructor(
    private location: Location,
    private formDragoon: DragoonCreateForm,
    private dashboardService: DashboardService) {
    this.formNewDragon = this.formDragoon.createForm();
  }

  public getDadosForm(): DragonModel {
    return Object.assign(
        this.formNewDragon.getRawValue()
    );
  }

  public saveNewDragoon(): void {
    this.dashboardService.createDragon(this.getDadosForm())
    .subscribe(() => {
      this.back();
    });
  }

  public back(): void {
    this.location.back();
  }

}
