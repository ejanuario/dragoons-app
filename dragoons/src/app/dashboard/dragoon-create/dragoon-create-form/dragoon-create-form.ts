import { Injectable } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Injectable()
export class DragoonCreateForm {

  constructor(private formBuilder: FormBuilder) { }

  public createForm(): FormGroup {
    return this.formBuilder.group({
      name: this.formBuilder.control('', [
        Validators.required
      ]),
      type: this.formBuilder.control('', [
        Validators.required
      ])
    });
  }
}
