import { Component, OnInit } from '@angular/core';

import { DragonModel } from './model/dragon.model';

import { DashboardService } from './../service/dashboard.service';

@Component({
  selector: 'app-dragoon-list',
  templateUrl: './dragoon-list.component.html',
  styleUrls: ['./dragoon-list.component.css']
})
export class DragoonListComponent implements OnInit {

  public dragoons: DragonModel[];

  constructor(private dashboardService: DashboardService) { }

  ngOnInit() {
    this.loadData();
  }

  public loadData(): void {
    this.dashboardService.getAllDragons()
      .subscribe(res => {
        this.dragoons = res;
      });
  }

  public deleteDragoon(id: string) {
    this.dashboardService.deleteDragon(id)
      .subscribe(() => {
        this.loadData();
    });
  }

}
