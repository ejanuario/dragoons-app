export class DragonModel {

  constructor(
    public id?: string,
    public name?: string,
    public type?: string,
    public histories?: any[],
    public createdAt?: string) { }

}
