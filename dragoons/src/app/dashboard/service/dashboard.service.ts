import { Injectable } from '@angular/core';

import { catchError } from 'rxjs/operators';
import { Observable, of } from 'rxjs';

import { DragonModel } from '../dragoon-list/model/dragon.model';

import { DashboardRestService } from './dashboard-rest.service';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(private dashboardRestService: DashboardRestService) { }

  public getAllDragons(): Observable<DragonModel[]> {
    return this.dashboardRestService.getAllDragons()
      .pipe(catchError(error => {
        console.error({error});
        return of([]);
      }));
  }

  public getDragonById(id: string): Observable<DragonModel> {
    return this.dashboardRestService.getDragonById(id)
      .pipe(catchError(error => {
        console.error({error});
        return of();
      }));
  }

  public createDragon(model: DragonModel): Observable<DragonModel> {
    return this.dashboardRestService.createDragon(model)
      .pipe(catchError(error => {
        console.error({error});
        return of();
      }));
  }

  public editDragon(id: string, model: DragonModel): Observable<DragonModel> {
    return this.dashboardRestService.editDragon(id, model)
      .pipe(catchError(error => {
        console.error({error});
        return of();
      }));
  }

  public deleteDragon(id: string): Observable<DragonModel> {
    return this.dashboardRestService.deleteDragon(id)
      .pipe(catchError(error => {
        console.error({error});
        return of();
      }));
  }

}
