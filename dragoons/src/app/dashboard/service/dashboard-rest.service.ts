import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable, of } from 'rxjs';

import { environment } from 'src/environments/environment.prod';

import { DragonModel } from '../dragoon-list/model/dragon.model';

@Injectable()
export class DashboardRestService {

  constructor(private http: HttpClient) { }

  public getAllDragons(): Observable<DragonModel[]> {
    return of({})
    .pipe(() => this.http.get<DragonModel[]>(`${environment.END_POINT}`));
  }

  public getDragonById(id: string): Observable<DragonModel> {
    return of({})
    .pipe(() => this.http.get<DragonModel>(`${environment.END_POINT}/${id}`));
  }

  public createDragon(model: DragonModel): Observable<DragonModel> {
    return of({})
    .pipe(() => this.http.post<DragonModel>(`${environment.END_POINT}`, model));
  }

  public editDragon(id: string, model: DragonModel): Observable<DragonModel> {
    return of({})
    .pipe(() => this.http.put<DragonModel>(`${environment.END_POINT}/${id}`, model));
  }

  public deleteDragon(id: string): Observable<DragonModel> {
    return of({})
    .pipe(() => this.http.delete<DragonModel>(`${environment.END_POINT}/${id}`));
  }

}
