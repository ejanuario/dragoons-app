import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

import { DragonModel } from '../dragoon-list/model/dragon.model';
import { DashboardService } from '../service/dashboard.service';

@Component({
  selector: 'app-dragoon-detail',
  templateUrl: './dragoon-detail.component.html',
  styleUrls: ['./dragoon-detail.component.css']
})
export class DragoonDetailComponent implements OnInit {

  public idDragoon: string;
  public dragoon: DragonModel;

  constructor(
    private location: Location,
    private activatedRoute: ActivatedRoute,
    private dashboardService: DashboardService) { }

  ngOnInit() {
    this.getDragonById();
  }

  private getDragonById(): void {
    this.idDragoon = this.activatedRoute.snapshot.paramMap.get('id');
    this.dashboardService.getDragonById(this.idDragoon)
      .subscribe(res => {
        this.dragoon = res;
      });
  }

  public back(): void {
    this.location.back();
  }

}
