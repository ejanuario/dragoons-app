import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';

import { FormGroup } from '@angular/forms';
import { DragonModel } from '../dragoon-list/model/dragon.model';
import { DragoonEditForm } from './dragoon-edit-form/dragoon-edit-form';

import { DashboardService } from './../service/dashboard.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-dragoon-edit',
  templateUrl: './dragoon-edit.component.html',
  styleUrls: ['./dragoon-edit.component.css']
})
export class DragoonEditComponent implements OnInit {

  public idDragoon: string;
  public dragoon: DragonModel;
  public formEditDragoon: FormGroup;

  constructor(
    private location: Location,
    private formDragoon: DragoonEditForm,
    private activatedRoute: ActivatedRoute,
    private dashboardService: DashboardService) {
      this.formEditDragoon = this.formDragoon.createForm();
    }

    ngOnInit(): void {
      this.getDragonById();
    }

    public getDadosForm(): DragonModel {
    return Object.assign(
        this.formEditDragoon.getRawValue()
    );
  }

  public editDragoon(): void {
    this.idDragoon = this.activatedRoute.snapshot.paramMap.get('id');
    this.dashboardService.editDragon(this.idDragoon, this.getDadosForm())
    .subscribe(() => {
      this.back();
    });
  }

  private getDragonById(): void {
    this.idDragoon = this.activatedRoute.snapshot.paramMap.get('id');
    this.dashboardService.getDragonById(this.idDragoon)
      .subscribe(res => {
        this.dragoon = res;
      });
  }

  public back(): void {
    this.location.back();
  }

}
