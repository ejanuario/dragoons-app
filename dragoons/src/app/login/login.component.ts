import { Router } from '@angular/router';
import { Component } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { AuthGuard } from '../auth-guard';
import { LoginForm } from './login-form/login-form';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  public form: FormGroup;
  public account: string;
  public password: string;

  constructor(
    private router: Router,
    private formLogin: LoginForm,
    private authGuard: AuthGuard) {
    this.form = this.formLogin.createForm();
  }

  getDataForm(): any {
    return Object.assign(
        this.form.getRawValue()
    );
  }

  login() {
    const data = this.getDataForm();
    this.account = data.account;
    this.password = data.password;
    if (this.account === 'dragons' && this.password === 'dragons123') {
      this.authGuard.validLogin();
      this.router.navigate(['/dashboard']);
    }
  }

}
