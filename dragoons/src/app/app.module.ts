import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';

import { AuthGuard } from './auth-guard';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { MzInputModule, MzValidationModule } from 'ngx-materialize';

import { DashboardService } from './dashboard/service/dashboard.service';
import { DashboardRestService } from './dashboard/service/dashboard-rest.service';

import { LoginForm } from './login/login-form/login-form';
import { DragoonEditForm } from './dashboard/dragoon-edit/dragoon-edit-form/dragoon-edit-form';
import { DragoonCreateForm } from './dashboard/dragoon-create/dragoon-create-form/dragoon-create-form';

import { LoginComponent } from './login/login.component';
import { HeaderComponent } from './header/header.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DragoonListComponent } from './dashboard/dragoon-list/dragoon-list.component';
import { DragoonEditComponent } from './dashboard/dragoon-edit/dragoon-edit.component';
import { DragoonCreateComponent } from './dashboard/dragoon-create/dragoon-create.component';
import { DragoonDetailComponent } from './dashboard/dragoon-detail/dragoon-detail.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HeaderComponent,
    DashboardComponent,
    DragoonEditComponent,
    DragoonListComponent,
    DragoonCreateComponent,
    DragoonDetailComponent
  ],
  imports: [
    FormsModule,
    MzInputModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    MzValidationModule,
    ReactiveFormsModule,
    BrowserAnimationsModule
  ],
  providers: [
    LoginForm,
    AuthGuard,
    DragoonEditForm,
    DashboardService,
    DragoonCreateForm,
    DragoonEditComponent,
    DashboardRestService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
