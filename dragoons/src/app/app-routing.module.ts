import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuard } from './auth-guard';

import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DragoonEditComponent } from './dashboard/dragoon-edit/dragoon-edit.component';
import { DragoonCreateComponent } from './dashboard/dragoon-create/dragoon-create.component';
import { DragoonDetailComponent } from './dashboard/dragoon-detail/dragoon-detail.component';

const routes: Routes = [
  { path: '', component: LoginComponent, canActivate: [AuthGuard] },
  { path: 'login', component: LoginComponent },
  { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard] },
  { path: 'create', component: DragoonCreateComponent, canActivate: [AuthGuard] },
  { path: 'edit/:id', component: DragoonEditComponent, canActivate: [AuthGuard] },
  { path: 'detail/:id', component: DragoonDetailComponent, canActivate: [AuthGuard] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
