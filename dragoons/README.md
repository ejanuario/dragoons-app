# LOGIN
dragons : dragons123

# Objetivo
Criar uma aplicação que contenha​: [OK]

# Página de login:

Única página disponível se não estiver logado; [OK]
Criar um usuário básico para acesso: ACCOUNT: dragons | PASSWORD: dragons123 [OK]
Uma página de lista de dragões: [OK]

Os nomes devem estar em ordem alfabética; [OK]
A partir da lista, deverá ser possível remover e alterar as informações dos dragões. [OK]
Uma página com os detalhes de um dragão específico: [OK]

# Os seguintes dados devem ser apresentados na página:

Data de criação; [OK]
Nome; [OK]
Tipo. [OK]
Uma página para cadastro de dragões: [OK]

3 Regras:

Layout responsivo; [OK]
Utilizar Angular 4+ para o desenvolvimento; [OK]
Usar um sistema de controle de versão para entregar o teste (Github, Bitbucket, ...) [OK]
O uso de libs é livre. [OK]

# O que será avaliado:

Organização do código; [OK]
Componentização das páginas; [OK]
Interface organizada e amigável; [OK]
Uso adequado do css (ou alguma biblioteca). [OK]

#API

http://5c4b2a47aa8ee500142b4887.mockapi.io/api/v1/dragon [OK]

GET .../api/v1/dragon [OK]
lista de dragões

GET .../api/v1/dragon/:id [OK]
detalhes de um draão

POST .../api/v1/dragon [OK]
criação de um dragão

PUT .../api/v1/dragon/:id [OK]
edição de um dragão

DELETE .../api/v1/dragon/:id [OK]
deleção de um dragão

# Dragoons

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.0.3.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
